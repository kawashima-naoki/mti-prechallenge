﻿document.getElementById("hello_text").textContent = "はじめてのJavaScript";

/** キーイベントを監視する */
document.addEventListener("keydown", onKeyDown);
document.addEventListener("keyup", onKeyUp);

var count = 0;   // 何回目かを数える
var cells;
var isGameOver = false;

// ブロックのパターン
var blocks = {
  i: {
    class: "i",
    pattern: [
      [1, 1, 1]
    ]
  },
  o: {
    class: "o",
    pattern: [
      [1, 1],
      [1, 1]
    ]
  },
  t: {
    class: "t",
    pattern: [
      [0, 1, 0],
      [1, 1, 1]
    ]
  },
  s: {
    class: "s",
    pattern: [
      [0, 1, 1],
      [1, 1, 0]
    ]
  },
  z: {
    class: "z",
    pattern: [
      [1, 1, 0],
      [0, 1, 1]
    ]
  },
  j: {
    class: "j",
    pattern: [
      [1, 0, 0],
      [1, 1, 1]
    ]
  },
  l: {
    class: "l",
    pattern: [
      [0, 0, 1],
      [1, 1, 1]
    ]
  },
  t2: {
    class: "t2",
    pattern: [
      [1, 1, 1],
      [0, 1, 0]
    ]
  },
  i2: {
    class: "i2",
    pattern: [
      [0, 1, 0],
      [0, 1, 0],
      [0, 1, 0]
    ]
  }
};

var process; // 現在実行中の処理

loadTable();
process = setInterval(main, 1000);

function main() {
  count++;
  // 何回目かを文字にまとめて表示する
  document.getElementById("hello_text").textContent = "はじめてのJavaScript("+ count +")";
  // ブロックが積み上がり切っていないかのチェック
  if (isGameOver) {
    alert("game over");
    return;
  }
  if (hasFallingBlocks()) { // 落下中のブロックがあるか確認する
    fallBlocks();
  } else { // なければ
    deleteRow(); // そろっている行を消す
    generateBlock(); // ランダムにブロックを生成する
  }
}

/* ----- ここから下は関数の宣言部分 ----- */

/** テーブルを取得する関数 */
function loadTable() {
  cells = [];
  var td_array = document.getElementsByTagName("td"); // 200個の要素を持つ配列
  var index = 0;
  for (var row = 0; row < 20; row++) {
    cells[row] = [];
    for (var col = 0; col < 10; col++) {
      cells[row][col] = td_array[index];
      index++;
    }
  }
}

/** ブロックを落とす関数 */
function fallBlocks() {
  // 底についていないか
  for (var col = 0; col < 10; col++) {
    if (cells[19][col].blockNum === fallingBlockNum) {
      isFalling = false;
      return; // 一番下の行にブロックがいるので落とさない
    }
  }
  // 1マス下に別のブロックがないか
  for (var row = 18; row >= 0; row--) {
    for (var col = 0; col < 10; col++) {
      if (cells[row][col].blockNum === fallingBlockNum) {
        if (cells[row + 1][col].className !== "" && cells[row + 1][col].blockNum !== fallingBlockNum) {
          isFalling = false;
          return; // 1マス下にブロックがいるので落とさない
        }
      }
    }
  }
  // 下から二番目の行から繰り返しクラスを下げていく
  for (var row = 18; row >= 0; row--) {
    for (var col = 0; col < 10; col++) {
      if (cells[row][col].blockNum === fallingBlockNum) {
        cells[row + 1][col].className = cells[row][col].className;
        cells[row + 1][col].blockNum = cells[row][col].blockNum;
        cells[row][col].className = "";
        cells[row][col].blockNum = null;
      }
    }
  }
}

/** 落下中のブロックがあるか確認する関数 */
var isFalling = false;
function hasFallingBlocks() {
  return isFalling;
}

/** そろっている行を消す関数 */
function deleteRow() {
  for (var row = 19; row >= 0; row--) {
    var canDelete = true;
    for (var col = 0; col < 10; col++) {
      if (cells[row][col].className === "") {
        canDelete = false;
      }
    }
    if (canDelete) {
      // 1行消す
      for (var col = 0; col < 10; col++) {
        cells[row][col].className = "";
      }
      // 上の行のブロックをすべて1マス落とす
      for (var downRow = row - 1; downRow >= 0; downRow--) {
        for (var col = 0; col < 10; col++) {
          cells[downRow + 1][col].className = cells[downRow][col].className;
          cells[downRow + 1][col].blockNum = cells[downRow][col].blockNum;
          cells[downRow][col].className = "";
          cells[downRow][col].blockNum = null;
        }
      }
    }
  }
}

/** ランダムにブロックを生成する関数 */
var fallingBlockNum = 0;
function generateBlock() {
  // ブロックを出現させられるか調べる
  for (var row = 0; row < 3; row++) {
    for (var col = 3; col < 6; col++) {
      if (cells[row][col].className !== "") {
        isGameOver = true;
        return;
      }
    }
  }

  // ブロックパターンからランダムに1つのパターンを選ぶ
  var keys = Object.keys(blocks);
  var nextBlockKey = keys[Math.floor(Math.random() * keys.length)];
  var nextBlock = blocks[nextBlockKey];
  var nextFallingBlockNum = fallingBlockNum + 1;
  // 選んだパターンをもとにブロックを配置する
  var pattern = nextBlock.pattern;
  for (var row = 0; row < pattern.length; row++) {
    for (var col = 0; col < pattern[row].length; col++) {
      if (pattern[row][col]) {
        cells[row][col + 3].className = nextBlock.class;
        cells[row][col + 3].blockNum = nextFallingBlockNum;
      }
    }
  }
  // 落下中のブロックがあるとする
  isFalling = true;
  fallingBlockNum = nextFallingBlockNum;
}

/** キー入力によってそれぞれの関数を呼び出す関数 */
function onKeyDown(event) {
  if (event.keyCode === 37) {
    moveLeft();
  } else if (event.keyCode === 39) {
    moveRight();
  }
  if (event.keyCode === 40) {
    clearInterval(process);
    process = setInterval(main, 100);
  }
}
function onKeyUp(event) {
  if (event.keyCode === 40) {
    clearInterval(process);
    process = setInterval(main, 1000);
  }
}

/** ブロックを右に移動させる関数 */
function moveRight() {
  // 動かすブロックが右端にいないか, 動かすブロックの右にブロックがないか
  for (row = 0; row < 20; row++) {
    if (cells[row][9].blockNum === fallingBlockNum) return;
    for (var col = 9; col >= 0; col--) {
      if (cells[row][col].blockNum === fallingBlockNum) {
        if (cells[row][col + 1].blockNum !== fallingBlockNum && cells[row][col + 1].className !== "") return;
      }
    }
  }
  // 右に1マス動かす
  for (var row = 0; row < 20; row++) {
    for (var col = 9; col >= 0; col--) {
      if (cells[row][col].blockNum === fallingBlockNum) {
        cells[row][col + 1].className = cells[row][col].className;
        cells[row][col + 1].blockNum = cells[row][col].blockNum;
        cells[row][col].className = "";
        cells[row][col].blockNum = null;
      }
    }
  }
}

/** ブロックを左に移動させる関数 */
function moveLeft() {
  // 動かすブロックが左端にいないか, 動かすブロックの左にブロックがないか
  for (row = 0; row < 20; row++) {
    if (cells[row][0].blockNum === fallingBlockNum) return;
    for (var col = 0; col < 10; col++) {
      if (cells[row][col].blockNum === fallingBlockNum) {
        if (cells[row][col - 1].blockNum !== fallingBlockNum && cells[row][col - 1].className !== "") return;
      }
    }
  }
  // 左に1マス動かす
  for (var row = 0; row < 20; row++) {
    for (var col = 0; col < 10; col++) {
      if (cells[row][col].blockNum === fallingBlockNum) {
        cells[row][col - 1].className = cells[row][col].className;
        cells[row][col - 1].blockNum = cells[row][col].blockNum;
        cells[row][col].className = "";
        cells[row][col].blockNum = null;
      }
    }
  }
}